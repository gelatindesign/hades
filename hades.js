/**
 * Hades
 *
 * A javascript 2D canvas library port of PyGame [http://www.pygame.org/]
 */

/**
 * Extend Function to allow easy inheritance
 * @author http://phrogz.net/JS/classes/OOPinJS2.html
 */
Function.prototype.extends = function( parentClassOrObject ) {
	if ( parentClassOrObject.constructor == Function ) {
		//Normal Inheritance
		this.prototype = new parentClassOrObject;
		this.prototype.constructor = this;
		this.prototype.parent = parentClassOrObject.prototype;
	} else {
		//Pure Virtual Inheritance
		this.prototype = parentClassOrObject;
		this.prototype.constructor = this;
		this.prototype.parent = parentClassOrObject;
	}
	return this;
};

(function() {

// Call Hades on the window
window.Hades = function( selector, parent ) {
	return Hades.core.init( selector, parent );
}

// Attach to $ if available
if ( !window.$ )
	window.$ = window.Hades;

/**
 * Hades.core
 * Contains the core functions
 */
Hades.core = {

	init: function( selector, parent ) {
		if ( typeof selector == 'function' ) {
			setTimeout(selector, 100);
			return;
		}

		var canvas = Hades.selector( selector, parent );
		//this._context = this._canvas.getContext( '2d' );
		return new Hades.surface( canvas );
	},

	error: function( message ) {
		throw new Error( message );
	},

	quit: function() {

	}
};

/**
 * Hades.loop
 * Contains the main game loop
 */
Hades.loop = function( loopMethod ) {
	setTimeout( function() {
		loopMethod();
		Hades.loop( loopMethod );
	}, 1);
};


/**
 * Hades.surface
 * Object for representing images
 */
Hades.surfaceMethods = {
	locked: false,

	clear: function() {
		this.fill( '#000000' );
	},

	// Draw a surface onto this surface
	blit: function( surface, dest ) { // area, flags

	},

	// Create a new copy of a surface
	copy: function() {

	},

	// Fill the surface with a solid color
	fill: function( color, rect ) {
		var x, y, w, h;

		color = color || '#000000';
		rect  = rect || null;

		this._context.fillStyle = color;

		this._context.beginPath();
		if ( rect == null ) {
			x = 0; y = 0;
			w = this.w; h = this.h;

		} else {
			x = rect.x; y = rect.y;
			w = rect.w; h = rect.h;
		}
		this._context.rect( x, y, w, h );
		this._context.closePath();

		this._context.fill();

		return new Hades.rect( x, y, w, h );
	},

	// Shift the surface image in place
	scroll: function( dx, dy ) {
		dx = dx || 0;
		dy = dy || 0;
	},

	// Get the color value at a single pixel
	getAt: function( x, y ) {
		x = x || 0;
		y = y || 0;

		x = parseInt( x );
		y = parseInt( y );

		this.getImageData();

		var pos = (y * this.w * 4) + (x * 4);

		return new Hades.colorValue( this.imageData[pos], this.imageData[pos+1], this.imageData[pos+2] );
	},

	getImageData: function() {
		if ( this.locked == false ) {
			this.image = this._context.getImageData( 0, 0, this.w, this.h );
			this.imageData = this.image.data;
			this.locked = true;
		}

		return this.imageData;
	}

	/*
	getAt: function( x, y, recache ) {
		x = x || 0;
		y = y || 0;
		recache = recache || false;

		if ( recache === true || this.imagedata == null ) {
			var pixels = this.w * this.h;
			var image = this._context.getImageData( 0, 0, this.w, this.h );
			this.imageData = image.data;
		}

		return new Hades.colorValue( 0, 0, 0 );

		// return new Hades.colorValue( this.imageData[4*], data[1], data[2] );
	}
	 */
};
Hades.surface = function( canvas ) {
	this._canvas  = canvas;
	this._context = canvas.getContext( '2d' );
	this.w       = this._canvas.width;
	this.h       = this._canvas.height;
	return this;
};
Hades.surface.extends( Hades.surfaceMethods );


/**
 * Hades.rect
 * Object for storing rectangular coordinates
 */
Hades.rectMethods = {

	move: function( offsetX, offsetY ) {
		this.x += offsetX;
		this.y += offsetY;

		// this.x = parseInt( this.x );
		// this.y = parseInt( this.y );
	},

	collidePoint: function( xPos, yPos ) {
		return (
			this.x <= xPos && this.x + this.w >= xPos &&
			this.y <= yPos && this.y + this.h >= yPos
		);
	},

	collideRect: function( rect ) {
		return this.collidePoint( rect.x, rect.y );
	},

	collideList: function( list ) {
		var c;
		for (i in list) {
			c = this.collideRect( list[i] );
			if (c) return true;
		}
		return false;
	}
};
Hades.rect = function ( xOrPos, yOrSize, w, h ) {
	if (typeof xOrPos == 'object' && typeof yOrSize == 'object') {
		this.x = xOrPos[0];
		this.y = xOrPos[1];
		this.w = yOrSize[0];
		this.h = yOrSize[1];
	} else if (typeof xOrPos == 'number' && typeof yOrSize == 'number') {
		this.x = xOrPos;
		this.y = yOrSize;
		this.w = w;
		this.h = h;
	} else {
		return false;
	}

	return this;
}
Hades.rect.extends( Hades.rectMethods );


/**
 * Hades.draw
 * Module for drawing shapes
 */
Hades.draw = {

	rect: function( surface, color, rect, width ) {
		width = width || 0;

		surface.fill( color, rect );
	},

	circle: function( surface, color, pos, radius ) {
		
	},

	ellipse: function( surface, color, rect, width ) {
		var x, y, w, h;

		width = width || 0;
		color = color || '#000000';
		rect  = rect || null;

		surface._context.fillStyle = color;

		surface._context.beginPath();
		if ( rect == null ) {
			x = 0; y = 0;
			w = this.w; h = this.h;

		} else {
			x = rect.x + rect.w/2; y = rect.y + rect.h/2;
			w = rect.w / 2; h = rect.h / 2;
		}
		surface._context.arc( x, y, w, 0, Math.PI*2, true );
		surface._context.closePath();

		surface._context.fill();

		return new Hades.rect( x, y, w, h );
	},

	line: function( surface, color, startPos, endPos, width ) {
		width = width || 0;
	}
};

/**
 * Hades.sprite
 */
Hades.sprite = {

	collide: function( sprite, group, doKill, callback ) {
		var collided = [];

		for ( s in group.sprites ) {
			if ( sprite.collideRect( group.sprites[s].rect ) ) {

				if ( doKill ) {
					group.remove( s );

				} else {
					collided.push( group.sprites[s] );
				}
			}
		}
	}
};

/**
 * Hades.sprite.group
 */
Hades.sprite.groupMethods = {

	copy: function() {

	},

	add: function( spriteOrList ) {

	},

	remove: function( keyOrSpriteOrList ) {

	},

	has: function( spriteOrList ) {

	},

	update: function() {

	},

	draw: function( surface ) {

	},

	clear: function( surface, background ) {

	},

	empty: function() {

	}
};
Hades.sprite.group = function( name ) {

}
Hades.sprite.group.extends( Hades.sprite.groupMethods );


/**
 * Hades.sprite.sprite
 */
Hades.sprite.spriteMethods = {

	update: function() {

	},

	add: function( groupOrList ) {

	},

	remove: function( groupOrList ) {

	},

	kill: function() {

	},

	alive: function() {

	},

	groups: function() {

	}
};
Hades.sprite.sprite = function( sprGrp ) {

}
Hades.sprite.sprite.extends( Hades.sprite.spriteMethods );


/**
 * Hades.image
 */
Hades.imageMethods = {


};
Hades.image = function( surface, src ) {
	this.loaded = false;
	
	var img = new Image();

	img.onload = function(e) {
		this.loaded = true;
		surface._context.drawImage( img, 0, 0 );
	};

	img.src = src;

	return this;
}
Hades.image.extends( Hades.imageMethods );


/**
 * Hades.color
 * Module for colors
 */
Hades.color = {

	random: function () {
		return new Hades.colorValue(
			parseInt(Math.random() * 255),
			parseInt(Math.random() * 255),
			parseInt(Math.random() * 255)
		);
	}
};
Hades.colorValueMethods = {
	toHex: function( ) {
		return '#' + this.toHexComponent( this.r ) + this.toHexComponent( this.g ) + this.toHexComponent( this.b );
	},

	toHexComponent: function( v ) {
		var hex = v.toString( 16 );
		return hex.length == 1 ? '0' + hex : hex;
	}
};
Hades.colorValue = function( r, g, b ) {
	this.r = parseInt(r);
	this.g = parseInt(g);
	this.b = parseInt(b);
}
Hades.colorValue.extends( Hades.colorValueMethods );



/**
 * Hades.selector
 */
Hades.selector = function( selector, context ) {
	var el, op, s;

	context = context || document;

	if ( typeof selector == "string" ) {
		op = selector.substr( 0, 1 ); // operator, # or .
		s  = selector.substr( 1 );

		switch (op) {

			// ID
			case '#':
				el = document.getElementById( s );
				el = ( Hades.isDescendant( el, context ) ) ? el : null;
				break;

			// Class
			case '.':
				els = document.getElementsByClassName( s );
				el = [];
				for ( var i = 0, max = els.length; i < max; i++) {
					if ( Hades.isDescendant( els[i], context ) ) {
						el.push( els[i] );
					}
				}
				break;


			default:
				el = context.getElementsByTagName( selector );
				break;
		}
	}

	return el;
};


/**
 * Hades.descendant
 */
Hades.isDescendant = function( el, ancestor ) {
	return ( ( el.parentNode == ancestor ) || ( ( el.parentNode != document ) && Hades.isDescendant( el.parentNode, ancestor ) ) );
}

/**
 * Hades.canvas
 * Contains the canvas functions
 */
Hades.canvas = {

	init: function( selector, parent ) {
		return this;
	},

	draw: function() {

	},

	drawRect: function( dimensions, position, color ) {

	},

	drawCircle: function( radius, position, color ) {

	}
};

/**
 * Hades.scene
 */
Hades.scene = {

	init: function( selector, parent ) {
		this.canvas = Hades.canvas( selector, parent );

		return this;
	}
};

})();