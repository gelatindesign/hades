# Hades

A javascript 2D canvas library port of [PyGame][1]

```js
// Initialise Hades on a canvas element
var surface = Hades( '#surface' );

// If no other library is included which conflicts, you can use the shorthand $ to access Hades
var surface = $( '#surface' );
```


## Surface

_Object for representing images_

```js
// New surfaces can be created, these are not drawn automatically to the canvas
var surface2 = new $.surface();

// Fill the surface with a solid colour
// Hades.surface.fill( colour [, Hades.rect = null] ): return Hades.rect
surface.fill( '#FF0000' );

// You can blit this surface onto the main canvas surface at the destination, e.g. picture-in-picture
surface.blit( surface2, [100, 100] );
```


## Rect

_Object for storing rectangular coordinates_

```js
// Create a rectangle at x,y with size w,h
// Hades.rect( x, y, w, h )
// Hades.rect( [x, y], [w, h] )
var rect = new $.rect( 0, 0, 5, 5 );
var rect2 = new $.rect( [10, 10], [20, 40] );

// Move the rect by an offset
// Hades.rect.move( offsetX, offsetY );
rect.move( 5, 10 );

// Test if a point is inside a rectangle
// Hades.rect.collidePoint( xPos, yPos )
var isCollided = rect.collidePoint( 10, 20 );

// Test if rectangle collides with another
// Hades.rect.collideRect( Hades.rect )
var isCollided = rect.collideRect( rect2 );
```


## Draw

_Module for drawing shapes_

```js
// Draw a rectangle onto a surface
// Hades.draw.rect( Hades.surface, colour, Hades.rect [, width = 0] )
$.draw.rect( surface, '#FF0000', rect );

// Draw a line onto a surface
// Hades.draw.line( Hades.surface, colour, startPos, endPos, width )
draw.line( surface, '#00FF00', [0, 0], [100, 100], 1 );
```


## Image

_Module for image loading_

```js
// Load new image from a file
// Hades.image( filename ): return Hades.surface
var img = new $.image( 'img/hades.png' );

// This new image is now a surface, so can be manipulated in the same way, e.g. cover it partly with a black box
img.fill( '#000000', $.rect( 0, 0, img.getWidth() / 2, img.getHeight() / 2 ) );
```


## Sprite

_Module with basic game object classes_

```js
// Create a sprite group, e.g. you may have a group for 'enemy' sprites and a group for 'friendly' sprites
var enemySprGrp = new $.sprite.Group();

// Create a new sprite
// Hades.sprite.Sprite( Hades.sprite.Group )
var spr = new $.sprite.Sprite( enemySprGrp );
spr.image = img;
spr.rect = img.getRect();

// You can extend this to create your own types of sprites
function Box( position ) {
	this.image = new $.image( 'img/box.jpg' );
	this.rect = this.image.getRect();
	this.rect.topleft = position;
}
Box.inheritsFrom( $.sprite.Sprite ); // .inheritsFrom() is an extension of the Function object by Hades

var myBox = new Box([20, 50]);
```


## Transform

_Module to transform surfaces_



## Mixer

_Module for loading and playing sounds_


[1]: http://www.pygame.org/